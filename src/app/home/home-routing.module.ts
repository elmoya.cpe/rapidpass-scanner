import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomePage } from './home.page';

import { ZXingScannerModule } from '@zxing/ngx-scanner';
import { ScannerComponent } from './scanner/scanner.component';

const routes: Routes = [
  {
    path: '',
    component: HomePage
  },
  {
    path: 'scan',
    component: ScannerComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes), ZXingScannerModule],
  exports: [RouterModule],
})
export class HomePageRoutingModule {}
