import { Component, OnInit, ViewChild } from '@angular/core';
import { ZXingScannerComponent } from '@zxing/ngx-scanner';

@Component({
  selector: 'app-scanner',
  templateUrl: './scanner.component.html',
  styleUrls: ['./scanner.component.scss'],
})

export class ScannerComponent implements OnInit {

  @ViewChild('ZXingScannerComponent', { static: false })
  scanner: ZXingScannerComponent;

  @ViewChild('result-overlay', { static: false })
  scanresult: HTMLDivElement;

  codeParsed = false;

  resultParsed;

  constructor() { }

  ngOnInit() {}

  scanSuccessHandler(code) {
    this.codeParsed = code;

    this.checkValidity(this.codeParsed);
  }

  checkValidity(code) {
    // test
    setTimeout(() => {
      
      this.showResult();

    }, 3000);
  }

  showResult() {
    
    if (this.resultParsed) {
      this.showSuccess();
    } else {
      this.showFailed();
    }
  }

  showSuccess() {
    
  }

  showFailed() {

  }

  dismissResultPage() {
    this.codeParsed = false;
  }

}
