import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PlateNumberCheckPage } from './plate-number-check.page';

const routes: Routes = [
  {
    path: '',
    component: PlateNumberCheckPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PlateNumberCheckPageRoutingModule {}
