import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PlateNumberCheckPageRoutingModule } from './plate-number-check-routing.module';

import { PlateNumberCheckPage } from './plate-number-check.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PlateNumberCheckPageRoutingModule
  ],
  declarations: [PlateNumberCheckPage]
})
export class PlateNumberCheckPageModule {}
